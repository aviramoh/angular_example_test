import { UsersService } from './../users.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'users-update-form',
  templateUrl: './users-update-form.component.html',
  styleUrls: ['./users-update-form.component.css']
})
export class UsersUpdateFormComponent implements OnInit {

  @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); //any is the type. could be [] or {}
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>(); //pessimistic


  service:UsersService;
  usrform = new FormGroup({
    name:new FormControl(),
    phone_number:new FormControl(),
  });
  constructor(private route: ActivatedRoute ,service: UsersService, private router: Router) {
    this.service = service;
   }

   sendData() {
    //הפליטה של העדכון לאב
  this.addUser.emit(this.usrform.value.name);
  console.log(this.usrform.value);
  this.route.paramMap.subscribe(params=>{
    let id = params.get('id');
    this.service.putUser(this.usrform.value, id).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
        this.router.navigateByUrl("/");//return to main page
      }
    );
  })
}
  user;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
    })
  }

}
