import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UsersService {
  http:Http;

  getUsers() {
    return this.http.get(environment.url +'users');
  }

  getMessagesFire(){
    return this.db.list('/users').valueChanges();
  }

  postUser(data){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name', data.name).append('phone_number', data.phone_number);
    return this.http.post(environment.url +'users', params.toString(), options);
    
  }

  deleteMessage(key){
    return this.http.delete(environment.url +'users/'+key);
  }

  getUser(id) {
    return this.http.get(environment.url +'users/' + id);
  }

  putUser(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('phone_number',data.phone_number);
    return this.http.put(environment.url +'users/'+ key,params.toString(), options);
  }

  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http
   }

}
