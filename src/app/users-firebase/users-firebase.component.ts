import { UsersService } from './../users/users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users-firebase',
  templateUrl: './users-firebase.component.html',
  styleUrls: ['./users-firebase.component.css']
})
export class UsersFirebaseComponent implements OnInit {

  users;

  constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getMessagesFire().subscribe(response=>{
      console.log(response);
      this.users = response;
    })
  }

}
